const express = require("express");
const mongoose = require("mongoose");

// Allows our backend application to be available to our front end application
// Cross-Origin Resource Sharing
const cors = require("cors");

// Allows access to routes defined within our application
const userRoutes = require("./routes/userRoutes");

const courseRoutes = require("./routes/courseRoutes");
const app = express();	

// MongoDB Connection
mongoose.connect("mongodb+srv://admin:admin@zuitt-course-booking.a1svtt9.mongodb.net/b244_booking?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

mongoose.connection.once(`open`, 	() =>
	console.log(`Now connected to the MongoDB Atlas.`))

app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Defines "/users" to be included for all the user routes defined in the userRoutes file
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

// Will use the define port number for the application whenever an environment variable is available or will use the port 4000 if none is defined
// This syntax will allow flexibility 
app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online at port ${process.env.PORT || 4000}`)
});
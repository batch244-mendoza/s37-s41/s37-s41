const express = require("express");
const auth = require("../auth");
const router = express.Router();
const userController = require("../controllers/userController");

// Route for checking if the user's email already exists in the database.
// Invokes the checkEmailExists function from the controller to communicate with our database.

router.post("/checkEmail", (req,res) => {

	// .then method uses the result from the controller functions and sends it back to the frontend application via res.send
	userController.checkEmailExists(req.body).then(resultFromController => res.send (resultFromController));
})

// Route for user registration
router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send (resultFromController))
});

// Route for the user authentication
router.post("/login", (req,res) => {
	userController.loginUser(req.body).then(resultFromController => res.send (resultFromController))
})

// solution
// the "auth.verify" acts as a middleware to ensure that the user is logged in first before they retrieve the detailes
router.get("/details", auth.verify, (req, res) => {

	// uses the "decode" method in the auth.js to retrieve the user info from the token, pasing the "token" from the request heeader as an argument
	const userData = auth.decode(req.headers.authorization);
	console.log(userData)

	// Provides the user's ID for the getProfile controller method
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});



//route to enroll a user to a course
router.post("/enroll", auth.verify, (req, res) => {

	let user = auth.decode(req.headers.authorization);

	let course = {
		userId : user.id,
		courseId : req.body.courseId,
	};

	 if (user.isAdmin === false){
		userController.enroll(course).then((resultFromController) => res.send(resultFromController));
	} else {
		return res.send(false)
	}
});





module.exports = router;

